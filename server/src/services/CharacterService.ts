import {Character} from '../entity/character'

interface Stats {
    dex: number,
    str: number,
    int: number,
    wis: number,
    const: number,
    char: number
}

interface Inventory {
    name: string
}

export interface CharacterArgs {
    class: string,
    race: string,
    stats: Stats,
    gold: number,
    inventory: Inventory[]
}

interface characterDto {
    characterName: string,
    characterDetails: CharacterArgs
}


export class CharacterService {


    public static async createNewCharacter(characterName: string, characterArgs: CharacterArgs): Promise<boolean> {

        const newCharacter = Character.create({
            characterName,
            characterDetails: characterArgs
        });

        await newCharacter.save();

        return true;
    }

    public static async getCharacter(characterName: string): Promise<characterDto> {

        let user: Character | undefined = await Character.findOne({
            where: {characterName: characterName},
            select: ["characterName", "characterDetails"]
        });

        if (!user) {
            throw new Error('Sorry my dude, no character with that name :( SAD DAYS!!');
        }

        let userDto = JSON.stringify(user);

        console.log(userDto);

        return {
            characterName: user.characterName,
            characterDetails: user.characterDetails
        };

    };
}