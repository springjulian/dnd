import * as bcrypt from 'bcryptjs';
import {User} from '../entity/user';

interface UserEntity {
    email: string,
    password: string,
}

export class UserService {
    private static userMap: Map<string, UserEntity> = new Map<string, UserEntity>();
    private static currentUsers: Map<string, string> = new Map<string, string>();

    public static async login (email: string, password: string): Promise<string> {
        let user = this.userMap.get(email);

        if (user && password === user.password) {
            this.currentUsers.set(email, email);
            return  `logined in`;
        }

        throw new Error("Incorrect login detals");


    }

    public static async Register (email: string, password: string, firstName: string): Promise<boolean> {
        const hashedPassword = await bcrypt.hash(password, 12);

        const user = User.create({
            email,
            password: hashedPassword,
            firstName
        });

        await user.save();
        return true;
    }

    public static async GetCurrentUsers (): Promise<string[]> {
        let allCurrentUsers: string[] = [];
        this.userMap.forEach( (value) => {
            allCurrentUsers.push(value.email);
        });

        return allCurrentUsers;
    }


}