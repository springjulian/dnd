import { UserService} from './services/UserService';
import {CharacterService} from "./services/CharacterService";
import { ResolverMap } from "./types/graphql-utils";

interface loginArgs {
    email: string,
    password: string,
}

interface registerArgs {
    email: string,
    password: string,
    firstName: string,
}


export const resolvers: ResolverMap = {

    Query: {
        login: async (_: any, {email, password}: loginArgs): Promise<string> => {
            return await UserService.login(email, password);
        },

        getCurrentUsers: async (_: any): Promise<string[]> => {
            return await UserService.GetCurrentUsers();
        },

        getCharacter: async (_: any, {characterName}: GQL.IGetCharacterOnQueryArguments): Promise<GQL.ICharacterReturn> => {
            console.log(await CharacterService.getCharacter(characterName));
            return await CharacterService.getCharacter(characterName);
        }
    },

    Mutation: {
        register: async (_ : any, {email, password, firstName}: registerArgs): Promise<boolean> => {
            return await UserService.Register(email, password, firstName);
        },

        createNewCharacter: async (_: any, {characterName, characterDetails}: any): Promise<boolean> => {
            return await CharacterService.createNewCharacter(characterName, characterDetails);
        }
    }

}