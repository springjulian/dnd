import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToMany,
    BaseEntity
} from "typeorm";
import {Character} from "./character";


@Entity("user")
export class User extends BaseEntity {

    @PrimaryGeneratedColumn() id: string;

    @Column("varchar", { length: 255 })
    email: string;

    @Column("text") password: string;

    @Column("text") firstName: string;

    /* tslint:disable:no-unused-parameters */
    @OneToMany(() => Character, photo => photo.user)
    photos: Character[];

}