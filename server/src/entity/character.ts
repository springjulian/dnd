import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    BaseEntity
} from "typeorm";

import {User} from './user';

import {CharacterArgs} from "../services/CharacterService";

@Entity("character")
export class Character extends BaseEntity {

    @PrimaryGeneratedColumn("uuid") id: string;

    @Column("text") characterName: string;

    @Column("json")
    characterDetails: CharacterArgs;

    @ManyToOne(() => User, (user: User) => user.photos)
    user: User;

}
