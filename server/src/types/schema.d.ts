// tslint:disable
// graphql typescript definitions

declare namespace GQL {
interface IGraphQLResponseRoot {
data?: IQuery | IMutation;
errors?: Array<IGraphQLResponseError>;
}

interface IGraphQLResponseError {
/** Required for all errors */
message: string;
locations?: Array<IGraphQLResponseErrorLocation>;
/** 7.2.2 says 'GraphQL servers may provide additional entries to error' */
[propName: string]: any;
}

interface IGraphQLResponseErrorLocation {
line: number;
column: number;
}

interface IQuery {
__typename: "Query";
login: string | null;
getCurrentUsers: Array<string | null> | null;
getCharacter: ICharacterReturn | null;
}

interface ILoginOnQueryArguments {
email?: string | null;
password?: string | null;
}

interface IGetCharacterOnQueryArguments {
characterName: string;
}

interface ICharacterReturn {
__typename: "characterReturn";
characterName: string | null;
characterDetails: ICharacterDetailsDTO | null;
}

interface ICharacterDetailsDTO {
__typename: "characterDetailsDTO";
name: string | null;
class: string | null;
race: string | null;
stats: IStatsDTO | null;
gold: number | null;
inventory: Array<IInventoryDTO | null> | null;
}

interface IStatsDTO {
__typename: "StatsDTO";
dex: number | null;
str: number | null;
int: number | null;
wis: number | null;
const: number | null;
char: number | null;
}

interface IInventoryDTO {
__typename: "InventoryDTO";
item: string | null;
}

interface IMutation {
__typename: "Mutation";
register: boolean | null;
createNewCharacter: boolean | null;
}

interface IRegisterOnMutationArguments {
email: string;
password: string;
firstName: string;
}

interface ICreateNewCharacterOnMutationArguments {
characterName?: string | null;
characterDetails?: ICharacterDetails | null;
}

interface ICharacterDetails {
name?: string | null;
class?: string | null;
race?: string | null;
stats?: IStats | null;
gold?: number | null;
inventory?: Array<IInventory | null> | null;
}

interface IStats {
dex?: number | null;
str?: number | null;
int?: number | null;
wis?: number | null;
const?: number | null;
char?: number | null;
}

interface IInventory {
item?: string | null;
}
}

// tslint:enable
