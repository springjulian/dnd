#!/usr/bin/env bash

readonly DB_IMAGE_NAME=mineintellect_mine-database;
readonly DB_CONTAINER_NAME=mineintellect_mine-database_1;

docker run --name=${DB_CONTAINER_NAME} -d -p 5432:5432 ${DB_IMAGE_NAME};

