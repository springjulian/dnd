#!/usr/bin/env bash

cd "$(dirname "$0")";

readonly DB_IMAGE_NAME=mineintellect_mine-database;

docker build -t ${DB_IMAGE_NAME} . && ./run-container.sh;